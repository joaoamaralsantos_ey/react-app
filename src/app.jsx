import React from 'react';
import './css/app.css';
import HeaderApp from './components/header.jsx';
import ContainerApp from './components/container.jsx';


const App = () => {
    return (
        <div className="app">
            <HeaderApp/>
            <ContainerApp/>
        </div> );
};


export default App;
