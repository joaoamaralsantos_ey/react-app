import axios from 'axios';

export async function fetchProjects(language) {
    
    const url =  `https://api.github.com/search/repositories?q=stars:%3E1+language:${language}&sort=stars&order=desc&type=Repositories`;
    
    try {
        const response = await axios.get(url);
        console.log(response);

        return  response.data.items.map( item => ({
            id: item.id,
            name:  item.name,
            owner: item.owner.login,
            image: item.owner.avatar_url,
            stars: item.stargazers_count,
            url: item.svn_url
        }) );

    } catch(error) {
        throw error;
    }
}