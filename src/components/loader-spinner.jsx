import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';

function LoaderSpinner(props) {
    return (
        <Dimmer className = {props.active} inverted>
            <Loader inverted>Loading</Loader>
        </Dimmer>
    );
}

export default LoaderSpinner;
