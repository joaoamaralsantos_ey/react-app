import React from 'react';
import Navbar from './navbar';
import GridProjects from './grid-projects';
import { fetchProjects } from '../services/fetch-service';
import LoaderSpinner from './loader-spinner';
import Error from './error';
import '../css/app.css';

class ContainerApp extends React.Component {    

    constructor(props) {
        super(props);

        this.selectLanguage= this.selectLanguage.bind(this); // bind 'this' in the constructor, instead of creating an instance function
    }

    state = {
        langSelected: 'All',
        languageItems: [],
        loadingState: 'active',
        hasError: false,
        error: {}
    }

    selectLanguage(lang) {

        const previousLanguage = this.state.langSelected;

        if(lang === previousLanguage) {
            return;
        }

        this.setState({ langSelected: lang, loadingState: 'active' });
        this.updateState(lang);    
    }
    
    async updateState(lang) {

        try{
            const repos = await fetchProjects(lang);
            
            this.setState( {languageItems: repos, loadingState: '', hasError: false});
    
        } catch(error) {
            this.setState( {hasError: true, error: error, loadingState: ''});
        }
    }

    componentDidMount() {
        this.updateState(this.state.langSelected); 
    }

    render() {
        //console.log('render');
        return (
            <div>
                <Navbar className = "navbar"  selected = {this.state.langSelected} clickHandler = {this.selectLanguage}/>
                <LoaderSpinner active = {this.state.loadingState} />
                {this.state.hasError === false 
                    ? <GridProjects className = "grid-projects" languageItems = {this.state.languageItems}/> 
                    : <Error error = {this.state.error}/> }
            </div>
        );
    }
}

export default ContainerApp;