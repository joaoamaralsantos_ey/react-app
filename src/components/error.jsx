import React from 'react';
import { Message } from 'semantic-ui-react';
import '../css/app.css';

function Error(props) {
    return (
        <Message error negative>
            <Message.Header>Something went wrong</Message.Header>
            <p>{props.error.message}</p>
        </Message>

    );
}

export default Error;