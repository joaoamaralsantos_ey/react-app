import React from 'react';
import { Card, Icon, Image } from 'semantic-ui-react';

function CardProject(props) {

    props = props.data;

    return(
        <Card>
            <Image src= {props.image} />
            <Card.Content>
                <Card.Header href = {props.url}>
                    {props.name} 
                </Card.Header>
                <Card.Description>
                    {props.owner}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <a>
                    <Icon name='star' />
                    {props.stars}
                </a>
            </Card.Content>
        </Card>
    );
}

export default CardProject;