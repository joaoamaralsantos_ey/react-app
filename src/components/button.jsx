import React from 'react';
import { Button } from 'semantic-ui-react';

function ButtonLanguage(props) {
    const color = props.active === true ? 'blue' : '';
    return <Button className={color} onClick={props.clickHandler}>{props.language}</Button>;
}

export default ButtonLanguage;