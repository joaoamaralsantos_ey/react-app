import React from 'react';
import { Button } from 'semantic-ui-react';
import ButtonLanguage from './button.jsx';
import '../css/app.css';

class Navbar extends React.Component {

    static languages = ['All', 'Javascript', 'Java', 'HTML', 'CSS', 'C', 'C++'];

    render() {
        return (
            <div className = {this.props.className}>
                <Button.Group>
                    {Navbar.languages.map(lang => {
                        let active = lang === this.props.selected ? true : false;
                        return <ButtonLanguage active={active} clickHandler={() => this.props.clickHandler(lang)} key={lang} language={lang} />;
                    })}
                </Button.Group>
            </div>
        );
    }
}

export default Navbar;