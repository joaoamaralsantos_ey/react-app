import React from 'react';
import { Header, Icon } from 'semantic-ui-react';
import '../css/app.css';

function HeaderApp(props) {
    return(
        <Header as='h2' icon className="app-header">
            <Icon name='github'/>
        Repos Finder
        </Header>
    );
}

export default HeaderApp;