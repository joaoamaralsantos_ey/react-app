import React from 'react';
import CardProject from './card-project.jsx';
import { Grid } from 'semantic-ui-react';

function GridProjects(props) {
    return (
        <div className = {props.className}>
            <Grid stackable columns={6} stretched>
                {props.languageItems.map( item => { 
                    return (
                        <Grid.Column key = {item.id}>
                            <CardProject data = {item} />
                        </Grid.Column>
                    );
                })}
            </Grid>
        </div>
    );
}

export default GridProjects;